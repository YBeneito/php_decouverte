<?php 


 echo('
 <div class="nav-header">
 <div class="brand-logo">
     <a href="home.php">
         <span class="logo-compact"><h1 class="text-white">Admin</h1></span>
         <span class="brand-title">
         <h1  class="text-white">Admin</h1>
         </span>
     </a>
 </div>
</div>
    <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">Menu</li>
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fas fa-database"></i><span class="nav-text">Base de donnée</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="./bdd.php">Consulter BDD</a></li>
                            <li><a href="./ajoutbdd.php">Ajouter un produit</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                        <i class="fas fa-user"></i> <span class="nav-text">Gestion de compte</span>
                        </a>
                        <ul aria-expanded="false">
                            <li><a href="./connexion.php">Connexion</a></li>
                            <li><a href="./email-read.html">WIP</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>');
?>