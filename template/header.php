<?php 

if (!isset($_SESSION['connected'])) {
    echo('
<!--**********************************
Nav header start
***********************************-->
<div class="nav-header">
<div class="brand-logo">
    <a href="index.html">
        <b class="logo-abbr"><img src="images/logo.png" alt=""> </b>
        <span class="logo-compact"><img src="./images/logo-compact.png" alt=""></span>
        <span class="brand-title">
            <img src="images/logo-text.png" alt="">
        </span>
    </a>
</div>
</div>
<!--**********************************
Nav header end
***********************************-->

<!--**********************************
Header start
***********************************-->
<div class="header">    
<div class="header-content clearfix">
    
    <div class="nav-control">
        <div class="hamburger">
            <span class="toggle-icon"><i class="fa fa-bars"></i></span>
        </div>
    </div>

         
            
    <div class="header-right mt-3">
    <a href="./connexion.php" class ="btn btn-rounded btn-success"> <span>Se connecter</span></a>
    <a href="./deconnexion.php" class ="btn btn-rounded btn-warning d-none"> <span>Se deconnecter</span></a>
    </div>
</div>
</div>
<!--**********************************
Header end ti-comment-alt
***********************************-->


')
;
}
elseif ($_SESSION["connected"] === true) {
    echo('
<!--**********************************
Nav header start
***********************************-->
<div class="nav-header">
<div class="brand-logo">
    <a href="index.html">
        <b class="logo-abbr"><img src="images/logo.png" alt=""> </b>
        <span class="logo-compact"><img src="./images/logo-compact.png" alt=""></span>
        <span class="brand-title">
            <img src="images/logo-text.png" alt="">
        </span>
    </a>
</div>
</div>
<!--**********************************
Nav header end
***********************************-->

<!--**********************************
Header start
***********************************-->
<div class="header">    
<div class="header-content clearfix">
    
    <div class="nav-control">
        <div class="hamburger">
            <span class="toggle-icon"><i class="fa fa-bars"></i></span>
        </div>
    </div>

         
            
    <div class="header-right mt-3">
    <a href="./connexion.php" class ="btn btn-rounded btn-success d-none"> <span>Se connecter</span></a>
    <a href="./deconnexion.php" class ="btn btn-rounded btn-warning"> <span>Se deconnecter</span></a>
    </div>
</div>
</div>
<!--**********************************
Header end ti-comment-alt
***********************************-->


')
;
}
