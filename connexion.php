<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Connexion</title>

  <!-- Chartist -->
  <link rel="stylesheet" href="./plugins/chartist/css/chartist.min.css">
  <link rel="stylesheet" href="./plugins/chartist-plugin-tooltips/css/chartist-plugin-tooltip.css">
  <!-- Custom Stylesheet -->
  <link href="css/style.css" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />


</head>

<body>
  <div id="preloader">
    <div class="loader">
      <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
      </svg>
    </div>
  </div>

  <!--**********************************
        Header start
    ***********************************-->

  <?php
  include("./template/header.php")
  ?>

  <!--**********************************
        End header start
    ***********************************-->

  <?php
  include("./template/sidebar.php")
  ?>

  <h1>Connexion</h1>
  <section class="d-flex justify-content-center mt-5">
    <form action="/decouverte/connected.php" method="POST">
      <div class="form-group">
        <label for="pseudo">Pseudo</label>
        <input type="pseudo" class="form-control" name="pseudo" placeholder="Votre pseudo">
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password" placeholder="Password">
      </div>
      <button type="submit" class="btn btn-primary">Se connecter</button>
    </form>
  </section>
  <?php
  include("./template/scriptsjs.php")
  ?>

</body>

</html>