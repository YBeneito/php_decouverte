<?
    $statement = $pdo->query("SELECT * FROM articles");
    $articles = ($statement->fetchAll());
    ?>
    <div class="table-responsive p-5">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">nom</th>
                <th scope="col">description</th>
                <th scope="col">prix</th>
            </tr>
        </thead>
        <tbody>
            <? foreach ($articles as $article) : ?>
                <tr>
                    <th scope="row">
                        <p><?= $article['id'] ?></p>
                    </th>
                    <td>
                        <p><?= $article['nom'] ?></p>
                    </td>
                    <td>
                        <p><?= $article['description'] ?></p>
                    </td>
                    <td>
                        <p><?= $article['prix'] ?></p>
                    </td>
                </tr>
            <? endforeach ?>
        </tbody>
    </table>
    </div>


    <!--

    <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th>1</th>
                                                <td>Kolor Tea Shirt For Man</td>
                                                <td><span class="badge badge-primary px-2">Sale</span>
                                                </td>
                                                <td>January 22</td>
                                                <td class="color-primary">$21.56</td>
                                            </tr>
                                            <tr>
                                                <th>2</th>
                                                <td>Kolor Tea Shirt For Women</td>
                                                <td><span class="badge badge-danger px-2">Tax</span>
                                                </td>
                                                <td>January 30</td>
                                                <td class="color-success">$55.32</td>
                                            </tr>
                                            <tr>
                                                <th>3</th>
                                                <td>Blue Backpack For Baby</td>
                                                <td><span class="badge badge-success px-2">Extended</span>
                                                </td>
                                                <td>January 25</td>
                                                <td class="color-danger">$14.85</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                -->